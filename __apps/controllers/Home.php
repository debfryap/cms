<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();		
		$this->load->view('site/inc_header');
		$this->load->view('site/inc_footer');
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$title['caption'] = "Dashboard | System Kasir";			
			$this->load->view('site/home/v_home',$data);			
		}
		else
		{
			redirect('login','refresh');
		}
				
	}
	function tables(){
		echo "tables";
	}
	function forms(){	
		$title['caption'] = "Dashboard | System Kasir";			
		$this->load->view('site/home/v_form');		
	}
	function logout(){
		$this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('login', 'refresh');
	}
	
}
