<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login','',TRUE);
	}
	public function index()
	{
		if(!$this->session->userdata('logged_in'))
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		   	$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_db');

		   	if($this->form_validation->run() == false)
		   	{
		   		$this->load->view('v_login');
		   	}
		   	else
		   	{
		   		redirect('home','refresh');
		   	}
		}
		else
		{
			redirect('home','refresh');
		}
		
	}
	function check_db($password){
		$username = $this->input->post('username');
		$result = $this->M_login->login($username,$password);

		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
					'id' => $row->userid,
					'username' => $row->username
				);
				$this->session->set_userdata('logged_in',$sess_array);
			}
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_db', 'Invalid username or password');
     		return false;
		}
	}

	
}
